package r2.motivation.mock

import r2.motivation.util.MotivationContants
import java.util.*

class Phrase(val description: String, val category: Int)

fun Int.random(): Int =  Random().nextInt(this)


class Mock {
    private  val all = MotivationContants.PHASE_FILTER.all
    private  val sun = MotivationContants.PHASE_FILTER.sun
    private  val happy = MotivationContants.PHASE_FILTER.happy

    private val mListFrases: List<Phrase> = listOf(
        Phrase("Não sabendo que era impossível, foi lá e fez.", happy),
        Phrase("Você não é derrotado quando perde, você é derrotado quando desiste!", happy),
        Phrase("Quando está mais escuro, vemos mais estrelas!", happy),
        Phrase("Insanidade é fazer sempre a mesma coisa e esperar um resultado happy.", happy),
        Phrase("Não pare quando estiver cansado, pare quando tiver terminado.", happy),
        Phrase("O que você pode fazer agora que tem o maior impacto sobre o seu sucesso?", happy),
        Phrase("A melhor maneira de prever o futuro é inventá-lo.", sun),
        Phrase("Você perde todas as chances que você não aproveita.", sun),
        Phrase("Fracasso é o condimento que dá sabor ao sucesso.", sun),
        Phrase("Enquanto não estivermos comprometidos, haverá hesitação!", sun),
        Phrase("Se você não sabe onde quer ir, qualquer caminho serve.", sun),
        Phrase("Se você acredita, faz toda a diferença.", sun),
        Phrase("Riscos devem ser corridos, porque o maior perigo é não arriscar nada!", sun)

    )

    fun getPhrase(value: Int): String{

        val filteRed = mListFrases.filter { it -> (it.category == value || value == all ) }

        val rand = filteRed.size.random()

        return filteRed[rand].description

    }

}