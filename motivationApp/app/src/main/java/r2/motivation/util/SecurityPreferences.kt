package r2.motivation.util

import android.content.*

import android.content.SharedPreferences

class SecurityPreferences(context: Context) {

    private val mSharedPreferences: SharedPreferences = context.getSharedPreferences("LOOL", Context.MODE_PRIVATE)

    fun storeString(key:String, value: String){
//        salvar string passando um valor
        mSharedPreferences.edit().putString(key, value).apply()
    }

    fun getStoreString(key: String): String{
//        retorna o valor da string criada
        return mSharedPreferences.getString(key, "")
    }
}