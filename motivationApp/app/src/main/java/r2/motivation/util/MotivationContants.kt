package r2.motivation.util

class MotivationContants private constructor(){

    object Key{
        val PERSON_NAME = "personName"
    }

    object  PHASE_FILTER{
        val all =   1
        val sun =   2
        val happy = 3
    }

}