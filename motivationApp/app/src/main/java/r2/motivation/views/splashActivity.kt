package r2.motivation.views

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_splash.*
import r2.motivation.R
import r2.motivation.util.MotivationContants
import r2.motivation.util.SecurityPreferences

class splashActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mSecurity: SecurityPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mSecurity = SecurityPreferences(this)

        buttonSave.setOnClickListener(this)
        verifyUserName()
    }

    override fun onClick(view: View){
        val id = view.id
        if (id == R.id.buttonSave){
            handleSave()
        }
    }

    private fun verifyUserName(){
        editName.setText(mSecurity.getStoreString(MotivationContants.Key.PERSON_NAME))
    }

    private fun handleSave() {

        val name: String = editName.editableText.toString()
        val constName = MotivationContants.Key.PERSON_NAME

        if (name.isEmpty()){
            Toast.makeText(this, getString(R.string.informeNome), Toast.LENGTH_LONG).show()
        }else{
            mSecurity.storeString(constName, name)

            val intent: Intent = Intent(this@splashActivity, MainActivity::class.java)
            startActivity(intent)

//        impede que volte pra acitivity anterior
//        finish()

        }

    }
}
