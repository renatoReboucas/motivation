package r2.motivation.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import r2.motivation.R
import r2.motivation.mock.Mock
import r2.motivation.util.MotivationContants
import r2.motivation.util.SecurityPreferences

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private  var mFilter: Int = MotivationContants.PHASE_FILTER.all
    private lateinit var mSecurityPreferences: SecurityPreferences
    private  val mMock = Mock()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mSecurityPreferences = SecurityPreferences(this)


//        Eventos
        setListeners()

//        Inicializa
        handleFilter(R.id.imageAll)
        refreshPhrase()
        verifyUserName()

    }


    override fun onClick(view: View){
        val id = view.id

        val listId = listOf(R.id.imageAll, R.id.imageSun, R.id.imageHappy)
        if (id in listId){
            handleFilter(id)
        }else if(id == R.id.buttonNewPhrase){
            refreshPhrase()
        }
    }

    private fun setListeners(){
        imageAll.setOnClickListener(this)
        imageSun.setOnClickListener(this)
        imageHappy.setOnClickListener(this)
        buttonNewPhrase.setOnClickListener(this)
    }

    private fun verifyUserName(){
        textUserName.text = mSecurityPreferences.getStoreString(MotivationContants.Key.PERSON_NAME)
    }


    private fun handleFilter(id: Int){

        imageAll.setImageResource(R.drawable.ic_all_un_selected)
        imageSun.setImageResource(R.drawable.ic_sun_unselected)
        imageHappy.setImageResource(R.drawable.ic_happy_unselected)

        if (id == R.id.imageAll){
            mFilter = MotivationContants.PHASE_FILTER.all
            imageAll.setImageResource(R.drawable.ic_all_selected)
        }else if(id == R.id.imageSun){
            mFilter = MotivationContants.PHASE_FILTER.sun
            imageSun.setImageResource(R.drawable.ic_sun_selected)
        }else if(id == R.id.imageHappy){
            mFilter = MotivationContants.PHASE_FILTER.happy
            imageHappy.setImageResource(R.drawable.ic_happy_selected)
        }
    }

    private fun refreshPhrase(){
        textUserPhrase.text = mMock.getPhrase(mFilter)
    }

}
